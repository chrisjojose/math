package com.example.anshad.math;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);


        Button btn = (Button)findViewById(R.id.btncheck);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                Math obj1 = new Math();


                EditText txtPrime = (EditText)findViewById(R.id.txtprime);

                String primestr  = txtPrime.getText().toString();
                int prime = Integer.parseInt(primestr);

                obj1.prime(prime);


                boolean isPrime;
                isPrime=obj1.prime(prime);

                if(isPrime==true) {

                    TextView textView4 = (TextView) findViewById(R.id.textView4);
                    textView4.setText("is a prime number");

                }else
                {
                    TextView textView4 = (TextView) findViewById(R.id.textView4);
                    textView4.setText("is not a prime number");


                }




            }
        });

    }
}
