Android application which checks whether 
1. given year is leap year or not.
2. given number is prime or not.
3. given string is palindrome or not.